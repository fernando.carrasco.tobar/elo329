#ifndef CVECTOR_H
#define CVECTOR_H
#include <ostream>
using namespace std;

class CVector {
  private:
    double x,y;
  public:
    CVector () {};
    CVector (double , double);
    CVector operator + (const CVector &) const;
    friend CVector operator * (double factor, CVector v);
    friend ostream & operator<< (ostream &, const CVector &);
};
#endif
